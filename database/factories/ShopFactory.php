<?php

use Faker\Generator as Faker;

$factory->define(App\Shop::class, function (Faker $faker) {
    return [
       'name'=> $faker->company,
       'description'=>$faker->text(200),
       'city'=>$faker->city,
       'state'=>$faker->state,
       'zip'=>$faker->postcode,
       'address'=>$faker->address,
       'lat'=>$faker->latitude(42.5,39.6),
       'lng' => $faker->longitude(19.428472, -99.127667)
    ];
});
